#include<iostream>
class Demo{
	int x=10;
	public:
	Demo(int x){
		this->x=x;
		std::cout<<"In constructor"<<std::endl;
	}
	~Demo(){
		std::cout<<"In destructor"<<std::endl;
	}

	friend void* operator new(size_t size){
		std::cout<<"Here"<<std::endl;
		return (void* )malloc(size);
	}
	 friend  void operator delete(void* ptr){
		std::cout<<"there"<<std::endl;
		free(ptr);
	}
	void getData(){
		std::cout<< x << std::endl;
	}

};
int main(){
	Demo* obj1=new Demo(20);
	obj1->getData();
	delete(obj1);
	return 0;
}


