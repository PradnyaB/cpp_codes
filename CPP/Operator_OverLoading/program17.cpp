#include<iostream>
class Demo{
	int x=10;
	public:
	Demo(int x){
		this->x=x;
	}
/*	friend int operator+(const Demo& obj1, const Demo& obj2){
		return obj1.x+obj2.x;
	}*/
//	int operator+(const Demo obj);
	int getData()const{
		return x;
	}
};
/*int Demo::operator+(const Demo obj){
	return this->x+obj.x;
}*/
int operator+(const Demo& obj1, const Demo& obj2){
	return obj1.getData()+obj2.getData();
}
int main(){
	Demo obj1(100);
	Demo obj2(50);
	std::cout<< obj1 + obj2<<std::endl;
	return 0;
}
