#include<iostream>
class Demo{
	int x=10;
	public:
	Demo(int x){
		this->x=x;
	}
	void getData(){
		std::cout<< x<< std::endl;
	}
	~Demo(){
		std::cout<<"In destructor"<<std::endl;
	}

	void* operator new(size_t size){
		void *ptr=malloc(size);
		return ptr;
	}

	void operator delete(void* ptr){
		free(ptr);
	}
};
int main(){
	Demo *obj=new Demo(50);
	obj->getData();

	delete(obj);
}



