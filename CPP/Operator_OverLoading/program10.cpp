//insertion operator overloading
#include<iostream>
class Demo{
	int x=10;
	int y=20;
	public:
	//using friend fun
	/*friend std::ostream& operator<<(std::ostream& out, const Demo& obj){
		out<<obj.x;
		return out;
	}*/
	int getData()const{
		return x;
	}
};
//using normal function
std::ostream& operator<<(std::ostream& out, const Demo& obj){
	out<<obj.getData();
	return out;
}

int main(){
	Demo obj1;
	std::cout<<obj1<<std::endl;
	return 0;
}
