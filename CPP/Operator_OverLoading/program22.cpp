//Overload Parenthesis operator
#include<iostream>
class Demo{
	int x=10;
	public:
	Demo(int x){
		this->x=x;
	}
	int operator()(int x, int y){
		return x+y;
	}

};
int main(){
	Demo obj(20);
	int res = obj(40,50);
	std::cout<<res<<std::endl;
	return 0;
}
