#include<iostream>
class Demo{
	int x=10;
	public:
	Demo(int x){
		this->x=x;
	}
	void getData(){
		std::cout<< x <<std::endl;
	}

	void* operator new(size_t size){
		//void *ptr=::operator new(size);  (global fun call)
		void *ptr= malloc(size);
		return ptr;
	}
};

int main(){
	Demo *obj=new Demo(50);
	obj->getData();
	return 0;
}
