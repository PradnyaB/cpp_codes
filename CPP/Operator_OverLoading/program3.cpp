#include<iostream>
class Demo{
	int x=10;
	public:
	Demo(int x){
		this->x=x;
	}
	friend int operator+(const Demo&, const Demo&);
	friend int operator+(const Demo& obj, int data){
		return obj.x + data;
	}
};
int operator+(const Demo& obj1, const Demo& obj2){
	std::cout<<"Here.."<<std::endl;
	return obj1.x+obj2.x;
}
int main(){
	Demo obj1(30);
	Demo obj2(40);

	std::cout<< obj1+obj2 <<std::endl;
	std::cout<< obj1+70 <<std::endl;
	 

	return 0;
}
