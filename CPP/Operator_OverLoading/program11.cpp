//Extraction operator overload
#include<iostream>
class Demo{
	int x=10;
	public:
		Demo(){
		}
	       	Demo(int x){
			this->x=x;
		
		 }
		/* By using friend fun
		friend std::istream& operator>>(std::istream& in, Demo& obj){
		       in>>obj.x;
		     
	       		return in;
	 	}*/
		void printData(){
			std::cout<< x <<std::endl;
		}

		int setData(){
			return x;
			
		}		
};

std::istream& operator>>(std::istream& in, Demo& obj){
	in>>obj.setData();
	return in;
}
int main(){
	Demo obj;
	std::cout<<"enter value"<<std::endl;
	std::cin>>obj;

	obj.printData();
	return 0;
}
