#include<iostream>
class Demo{
	int x;
	public:
	Demo(int x){
		this->x=x;
		std::cout<<"In constructor"<<std::endl;

	}
	~Demo(){
		std::cout<<"In destructor"<<std::endl;
	}


	
};
int main(){
	Demo obj1(20);
	std::cout<<"In main"<<std::endl;

	std::cout<<"End main"<<std::endl;

	return 0;
}
