//Relational Operator overload: == operator
#include<iostream>
class Demo{
	int x=10;
	int y=20;
	public:
	Demo(int x){
		this->x=x;
	}

	friend int operator<(const Demo& obj1, const Demo& obj2){
		return obj1.x<obj2.x;
	}
};
int main(){
	Demo obj1(30);
	Demo obj2(60);
	std::cout<< (obj1< obj2)<< std::endl;
	return 0;
}
