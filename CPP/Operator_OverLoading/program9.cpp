//Division operator overload
#include<iostream>
class Demo{
	int x=0;
	public:
	Demo(int x){
		this->x=x;
	}
	//By using member fun
	int operator/(const Demo& obj){
		return this->x/obj.x;
	}
/* By using friend fun
	friend int operator/(const Demo& obj1, const Demo& obj2){
		return obj1.x/obj2.x;
	}*/

	int getData()const{
		return x;
	}
};
/*By using normal fun
int operator/(const Demo& obj1, const Demo& obj2){
	return obj1.getData()/obj2.getData();
}*/

int main(){
	Demo obj1(100);
	Demo obj2(20);

	std::cout<< obj1/obj2<<std::endl;

	return 0;
}

