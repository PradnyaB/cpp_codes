#include<iostream>
class Demo{
	int x=10;

	public:
	Demo(){
		std::cout<<"In constructor"<<std::endl;
	}

	friend std::ostream& operator<<(const std::ostream& cout, const Demo& obj);
};

std::ostream& operator<<(const std::ostream& cout, const Demo& obj){
	return	obj.x;
}

int main(){
	Demo obj;
	std::cout<< obj<<std::endl;

	return 0;
}
