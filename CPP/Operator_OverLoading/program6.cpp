#include<iostream>
class Demo{
	int x=10;
	int y=0;

	public:
	Demo(int x, int y){
		this->x=x;
		this->y=y;
	}
	int getData()const{
		return x;
	}
};

int operator+(const Demo& obj1, const Demo& obj2){
	return obj1.getData() +obj2.getData();
}

int main(){
	Demo obj1(10,20);
	Demo obj2(30,40);

	std::cout<< obj1 + obj2<< std::endl;
}
