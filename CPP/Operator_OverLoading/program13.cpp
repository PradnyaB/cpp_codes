//Relational opearator: less than with normal fun
#include<iostream>
class Demo{
	int x=10;
//	int y=20;

	public:
	Demo(int x){
		this->x=x;
//		this->y=y;

	}

	int getX()const{
		return x;
	}
};

int operator<(const Demo& obj1, const Demo& obj2){
	return obj1.getX()<obj2.getX();
}
int main(){
	Demo obj1(500);
	Demo obj2(100);
	std::cout<<(obj1<obj2)<<std::endl;
	return 0;
}


