#include<iostream>
class Parent{
	public:
		virtual void getData(int x){
			std::cout<<"In Parent getData"<<std::endl;
		}
};
class Child final:public Parent{
	public:
		void getData(short x)override {
			std::cout<<"In Child getData"<<std::endl;
		}
};
int main(){
	Parent *obj1=new Child();
	obj1->getData(10);
	return 0;
}
