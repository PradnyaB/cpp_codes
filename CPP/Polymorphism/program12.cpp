#include<iostream>
class Demo{
	Demo(){
	}
	public:
	

	static Demo* Singleton();
};
Demo* Demo::Singleton(){
	Demo* obj=new Demo();
	return obj;
}
int main(){
	Demo *obj=Demo::Singleton();
	Demo *obj1=Demo::Singleton();
	Demo *obj2=Demo::Singleton();
	Demo *obj3=Demo::Singleton();
	std::cout<<*obj<<std::endl;
	std::cout<<*obj1<<std::endl;
	std::cout<<*obj2<<std::endl;
	std::cout<<*obj3<<std::endl;
	return 0;
}
