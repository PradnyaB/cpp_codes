#include<iostream>
class Parent{
	public:
		virtual void getData() final{    //Error
			std::cout<<"In Parent getData"<<std::endl;
		}
};
class Child final:public Parent{
	public:
		void getData(){
			std::cout<<"In Child getData"<<std::endl;
		}
};
class Child2:public Child{    //Error
};
int main(){
	Parent *obj1=new Child();
	obj1->getData();
	return 0;
}
