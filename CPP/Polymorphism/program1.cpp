#include<iostream>
class Parent{
	public:
		Parent(){
			std::cout<<"Parent Constructor"<<std::endl;
		}
		virtual void getData(){
			std::cout<<"Parent getDAta"<<std::endl;
		}

};
class Child:public Parent{
	public:
		Child(){
			std::cout<<"Child Constructor"<<std::endl;
		}
		void getData(){                       //implicit virtual
			std::cout<<"Child getDAta"<<std::endl;
		}
};
int main(){
	Parent *obj=new Child();
	obj->getData();
	return 0;
}
