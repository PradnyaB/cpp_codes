#include<iostream>
class Demo{
	public:
		Demo(){
			std::cout<<"Demo Constructor"<<std::endl;
		}
};
class DemoChild1:virtual public Demo{
		public:
		DemoChild1(){
			std::cout<<"DemoChild1 Constructor"<<std::endl;
		}

};
class DemoChild2: virtual public Demo{
		public:
		DemoChild2(){
			std::cout<<"DemoChild2 Constructor"<<std::endl;
		}
	
};
class Child:public DemoChild1,public DemoChild2{
		public:
		Child(){
			std::cout<<"Child Constructor"<<std::endl;
		}
};
int main(){
	Child obj;
	
	return 0;
}
