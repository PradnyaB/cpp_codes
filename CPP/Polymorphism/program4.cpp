#include<iostream>
class Parent{
	public:
	
	       virtual Parent* getData(){
			std::cout<<"In Parent getData"<<std::endl;
			return new Parent();
		}

};
class Child:public Parent{
	public:
		Child* getData(){
			std::cout<<"In child getData"<<std::endl;
		
			return new Child();
		}
};
int main(){
	Parent *obj1=new Child();

	obj1->getData();
	return 0;
}
