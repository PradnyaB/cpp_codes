#include<iostream>
class Parent{
	public:
		virtual void getData(int x)const {
			std::cout<<"In Parent getData"<<std::endl;
		}
};
class Child final:public Parent{
	public:
		void getData(int x)const override {
			std::cout<<"In Child getData"<<std::endl;
		}
};
int main(){
	Parent *obj1=new Child();
	obj1->getData(10);
	return 0;
}
