#include<iostream>
class Parent{
	public:
		//void *_vptr;
		virtual void getData(){
			std::cout<<"Parent getData"<<std::endl;
		}
		virtual void printData(){
			std::cout<<"Parent printData"<<std::endl;
		}
};
class Child:public Parent{
	public:
		void getData(){
			std::cout<<"Child getData"<<std::endl;
		}
	
};
class Child2:public Parent{
	void printData(){
		std::cout<<"Child2 getdata"<<std::endl;
	}
};
int main(){
	Parent *obj1=new Child();
	obj1->getData();
	obj1->printData();
	return 0;
}
