#include<iostream>
class Demo{
	public:
		void getData(){
			std::cout<<"Demo::getData"<<std::endl;
		}
};
class DemoChild1:virtual public Demo{

};
class DemoChild2:virtual public Demo{
	
};
class Child:public DemoChild1,public DemoChild2{
};
int main(){
	Child obj;
	obj.getData();
	return 0;
}
