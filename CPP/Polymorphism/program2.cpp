#include<iostream>
class Parent{
	public:
		Parent(){
			std::cout<<"Parent Constructor"<<std::endl;
		}
		virtual void getData(int x){
			std::cout<<"Parent getDAta"<<std::endl;
		}
		virtual void printData(float x){
			std::cout<<"Parent PrintData"<<std::endl;
		}

};
class Child:public Parent{
	public:
		Child(){
			std::cout<<"Child Constructor"<<std::endl;
		}
		void getData(short x){                       //implicit virtual
			std::cout<<"Child getDAta"<<std::endl;
		}
		virtual void printData(float x){
			std::cout<<"Child PrintData"<<std::endl;
		}
};
int main(){
	std::cout<<"By Type 1"<<std::endl;
	Parent *obj=new Child();
	obj->getData(10);
	obj->printData(10.5f);
	std::cout<<std::endl;

	std::cout<<"By Type 2"<<std::endl;
	Child obj1;
	Parent *obj2=&obj1;
	obj2->getData(20);
	obj2->printData(20.5f);
	std::cout<<std::endl;

	std::cout<<"By Type 3 Reference"<<std::endl;
	Child obj3;
	Parent& obj4=obj3;
	obj4.getData(30);
	obj4.printData(30.5f);
	return 0;
}
