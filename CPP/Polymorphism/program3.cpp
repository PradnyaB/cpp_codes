#include<iostream>
class Parent{
	public:
		Parent(){
		std::cout<<this<<std::endl;
		}
		virtual int getData(){
		std::cout<<this<<std::endl;
			std::cout<<"In Parent getData"<<std::endl;
			return 30;
		}

};
class Child:public Parent{
	public:
		virtual int getData(){
		std::cout<<this<<std::endl;
			std::cout<<"In child getData"<<std::endl;
			return 10.5f;
		}
};
int main(){
	Parent *obj1=new Child();
	std::cout<<obj1<<std::endl;
	obj1->getData();
	return 0;
}
