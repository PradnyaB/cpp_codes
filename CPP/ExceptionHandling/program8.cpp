#include<iostream>
#include<exception>
class InvalidIndex: public std::runtime_error{
	
	public:
	InvalidIndex(std::string excep): std::runtime_error(excep){
		
	}
};
class Demo{
	int arr[5]={10,20,30,40,50};
	public:
	int arrlength(){
		return (sizeof(arr)/sizeof(arr[0]));
	}
	int operator[](int index){
		if(index<0 || index>=arrlength())
			throw InvalidIndex("Bad Index");

		return arr[index];
	}
};
int main(){
	Demo obj;
	try{
		std::cout<< obj[-3] <<std::endl;
	}catch(InvalidIndex& obj){
		std::cout<<"Exception occured"<< obj.what()<<std::endl;
	}
	
	return 0;
}

