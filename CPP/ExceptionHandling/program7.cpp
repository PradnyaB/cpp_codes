#include<iostream>
class InvalidIndex{
	std::string excep;
	public:
	InvalidIndex(std::string excep){
		this->excep=excep;
	}
	InvalidIndex(const InvalidIndex& ref){
		std::cout<<"copy"<<std::endl;
	}
	std::string getException(){
		return excep;
	}
};
class Demo{
	int arr[5]={10,20,30,40,50};
	public:
	int arrlength(){
		return (sizeof(arr)/sizeof(arr[0]));
	}
	int operator[](int index){
		if(index<0 || index>=arrlength())
			throw new InvalidIndex("Bad Index");

		return arr[index];
	}
};
int main(){
	Demo obj;
	try{
		std::cout<< obj[-3] <<std::endl;
	}catch(InvalidIndex *obj){
		std::cout<<"Exception occured"<< obj->getException()<<std::endl;
	}
	
	return 0;
}

