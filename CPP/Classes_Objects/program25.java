class Demo{
	int x=10;
	Demo(){
		System.out.println(x);
		this.x=100;
		System.out.println("In no args");
		System.out.println(x);
	}

	Demo(int x){
		this();

		System.out.println(this.x);
		this.x=x;
		System.out.println("In para ");
		System.out.println(this.x);
	}
		
		

}
class client{
	public static void main(String[] args){
		Demo obj=new Demo(50);

	}
}

