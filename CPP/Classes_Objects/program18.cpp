#include<iostream>
class Demo{
	public:
	int x=10;

	public:
	Demo(){
		std::cout<<"In No args Constructor"<<std::endl;
	}

	Demo(int x){
		this->x=x;
		std::cout<<"In para Constructor"<<std::endl;
	}

	Demo(Demo &xyz){

		std::cout<<"In copy Constructor"<<std::endl;
	}

	void fun(){
		std::cout<< x <<std::endl;
	}
};

int main(){

	Demo obj1{};

//	Demo obj2(20);

	Demo obj3=obj1;

	std::cout<<obj3.x <<std::endl;
	std::cout<<obj1.x <<std::endl;

	obj3.x=20;
	std::cout<<obj3.x <<std::endl;
	std::cout<<obj1.x <<std::endl;

	return 0;

}
