#include<iostream>
int main(){
	int x =10;
	int &y = x;
	int *ptr = &x;
	std::cout<< x <<std::endl; //10
	std::cout<< y <<std::endl;// 10
	std::cout<< ptr <<std::endl;//address of x as 1000

	std::cout<< &x <<std::endl;//address of x as 1000
	std::cout<< &y <<std::endl;//address of x as 1000
	std::cout<< &ptr <<std::endl;//address of ptr

	return 0;
}
