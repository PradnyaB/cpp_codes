#include<iostream>
class Two;
class One{
	int x=10;

	protected:
	int y=20;

	public:
	One(){
		std::cout<<"In One Constructor"<<std::endl;
	}
	public:
	void accessData(const Two& obj);

	

};
class Two{

	int x=40;

	protected:
	int y=20;

	public:
	Two(){
		std::cout<<"In Two constructor"<<std::endl;
	}
	friend void One::accessData(const Two& obj);
};
void Two::accessData(const One& obj){
		std::cout<< obj.x <<std::endl;
		std::cout<< obj.y <<std::endl;
}
int main(){
	One obj1;

	Two obj2;
	obj2.accessData(obj1);

	return 0;
}



