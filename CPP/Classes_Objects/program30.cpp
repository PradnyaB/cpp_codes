#include<iostream>
class Two;
class one{
	int x=10;
	protected:
       	int y=20;

	public:
	one(){
		std::cout<<"In one constructor"<<std::endl;
	}
	void getData()const{
		std::cout<< "Values of one class" <<std::endl;
		std::cout<< x <<std::endl;
		std::cout<< y <<std::endl;
	}
	friend void accessData(const one& obj1, const Two& obj2);

};

class Two{
	int x=50;
	protected:
       	int y=40;

	public:
	Two(){
		std::cout<<"In Two constructor"<<std::endl;
	}
	void getData()const{
		std::cout<< "Values of Two class" <<std::endl;
		std::cout<< x <<std::endl;
		std::cout<< y <<std::endl;
	}
	friend void accessData(const one& obj1, const Two& obj2);
};

void accessData(const one& obj1, const Two& obj2){

	obj1.getData();
	obj2.getData();
}

int main(){
	one obj1;
	Two obj2;

	accessData(obj1,obj2);
}



