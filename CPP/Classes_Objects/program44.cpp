#include<iostream>
class Player{
	int jrNo;
	std::string name;
	public:
	Player(int jrNo, std::string name){
		this->jrNo= jrNo;
		this->name = name;
	}
	void info(){
		std::cout<<jrNo <<" "<<std::endl;
		std::cout<<name <<" "<<std::endl;
	}
};
int main(){
	Player vk{18,"Virat"};
	vk.info();
	return 0;
}
