#include<iostream>
class Demo{
	public:
	Demo(){
		std::cout<<"In Constructor"<<std::endl;
	}
	void fun(){
		std::cout<<"In Fun "<<std::endl;
	}

};
int main(){
	std::cout<< "In main"<<std::endl;
	Demo obj;

	Demo *obj1 = new Demo();
	obj.fun();
	obj1->fun();

	delete obj1;
	obj1->fun();

	return 0;
}

