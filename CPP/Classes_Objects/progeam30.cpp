#include<iostream>
class One{
	int x=10;
	protected:
	int y=20;

	public:
	One(){
		std::cout<< "In one constructor"<<std::endl;
	}

	void accesData(){
		std::cout<< x <<std::endl;
		std::cout<< y <<std::endl;
	}


	friend class Two;

};

class Two{
	int a=40;

	protected:
	int b=50;

	public:
	Two(){
		std::cout<<"In Two constructor"<<std::endl;
	}
	private:
	void getData(const One& obj){
		std::cout<< obj.x<<std::endl;
		std::cout<< obj.y<<std::endl;
	}
	public:
	void accessData(const One& obj){
		std::cout<< obj.x<<std::endl;
		std::cout<< obj.y<<std::endl;
		getData(obj);
	}



};

int main(){

	One obj1;

	Two obj2;
	obj2.accessData(obj1);

	return 0;
}

