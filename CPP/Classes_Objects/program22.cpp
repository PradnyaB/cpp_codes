#include<iostream>
class Demo{
	public:
	Demo(){
		std::cout<<"In no args"<<std::endl;
	}
	Demo(int x){
		std::cout<<"In para"<<std::endl;
	}
	Demo(Demo &obj){
		std::cout<<"In copy"<<std::endl;
	}
};
Demo fun(){

	Demo obj4(50);

	return obj4;

}
int main(){
	Demo obj1;

	Demo obj2=obj1;
	Demo obj3=fun();


	return 0;

}

