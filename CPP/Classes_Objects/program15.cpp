#include<iostream>
class Demo{
	public:
		Demo(){
			std::cout<<"In no args Constructor"<<std::endl;
		}

		Demo(int x){
			std::cout<<"In para Constructor"<<std::endl;
		}

		Demo(Demo &xyz){
			std::cout<<"In copy Constructor"<<std::endl;
		}

};

int main(){
	Demo obj;

	Demo *obj2=new Demo(10);

	Demo obj3(*obj2);

	Demo obj4=obj;
	return 0;
	
}

