#include<iostream>
class Demo{
	public:
		int x=10;
		static int y;

		void fun();
};
int Demo::y=20;

void Demo::fun(){
	std::cout<<"x="<<x<<"  y="<<y<<std::endl;
}

int main(){
	Demo obj;
	Demo obj2;

	obj.fun();

	obj2.fun();

	std::cout<<"After change value by first object"<<std::endl;
	obj.x=50;
	obj.y=100;

	obj.fun();
	obj2.fun();
}

