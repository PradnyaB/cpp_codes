#include<iostream>
class Demo{
	public:
	Demo(){
		std::cout<<"In constructor"<<std::endl;
	}

	~Demo(){
		std::cout<<"In destructor"<<std::endl;
	}
};
int main(){
	Demo obj1;

	Demo *obj2=new Demo();
	delete obj2;

	std::cout<<"End main"<<std::endl;
	return 0;
}
