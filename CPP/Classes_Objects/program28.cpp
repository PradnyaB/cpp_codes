#include<iostream>
class Demo{
	int *ptr=NULL;
	public:
		Demo(){
			ptr=new int[50];
			std::cout<<"In constructor"<<std::endl;
		}
		~Demo(){
			delete[] ptr;
			std::cout<<"In destructor"<<std::endl;
		}

};
int main(){
	Demo obj1;

	return 0;
}


