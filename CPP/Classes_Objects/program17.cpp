#include<iostream>
class Demo{
	int x=10;
	int y=20;

	public:

	Demo(){
		std::cout<<"in no args Constructor "<<std::endl;
	}

	Demo(int x, int y){
		std::cout<<"in para Constructor "<<std::endl;
		this->x=x;
		this->y=y;
	}

	Demo(Demo &xyz){
		std::cout<<"In copy constructor"<<std::endl;
	}

	void disp(){
		std::cout<<x<<std::endl;
		std::cout<<y<<std::endl;
	}

};

int main(){

	Demo obj1;

	Demo obj2(1000,2000);

	Demo obj3=obj1;

	Demo obj4;

	obj4=obj1;

	obj1.disp();
	obj2.disp();
	obj3.disp();
	obj4.disp();
}

