#include<iostream>
class Demo {
	public:
	Demo(){
		std::cout<<"In no args"<<std::endl;
	}
	Demo(int x){

		std::cout<<"In para"<<std::endl;
	}
	Demo(Demo &obj){

		std::cout<<"In copy"<<std::endl;
	}

};

int main(){
	Demo* obj1=new Demo();
	Demo* obj2=new Demo();
	Demo* obj3=new Demo();
	Demo* obj4=new Demo();

	Demo arr[]={*obj1,*obj2,*obj3,*obj4};
	return 0;
}
