//forwardList
#include<iostream>
#include<forward_list>
int main(){
	std::forward_list<int> fw ={10,20,30,40,50};
	for(int& data: fw){
		std::cout<< data << std::endl;
	}
	std::forward_list<int>::iterator itr;
	fw.insert_after(fw.before_begin(),5);
	for(itr=fw.before_begin();itr!=fw.end();itr++){
		std::cout<< *itr <<std::endl;
	}

	return 0;
}
