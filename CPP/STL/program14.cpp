//Map
#include<iostream>
#include<map>
int main(){
	//way 1
	std::map<int,std::string> player;
	player[18] ="virat";
	player[7] ="MSD";
	player[18] ="virat dup";
	player[45] ="Rohit";
	std::map<int,std::string>::iterator itr;
	for(itr=player.begin();itr!=player.end();itr++){
		std::cout<< itr->first <<":";
		std::cout<<(*itr).second <<std::endl;
	}
	//way 2
	std::map<std::string,std::string> FriendInfo={{"Ashish","Barclays"},{"Kanha","Infosys"},{"Rahul","BMC"}};

	std::map<std::string,std::string>::iterator itr1;
	for(itr1=FriendInfo.begin();itr1!=FriendInfo.end();itr1++){
		std::cout<< itr1->first <<":";
		std::cout<<(*itr1).second <<std::endl;
	}
	std::cout<<"In Reverse Order:"<<std::endl;
	std::map<std::string,std::string,std::greater<std::string>> FriendInfo1={{"Ashish","Barclays"},{"Kanha","Infosys"},{"Rahul","BMC"}};

	std::map<std::string,std::string>::iterator itr2;
	for(itr2=FriendInfo1.begin();itr2!=FriendInfo1.end();itr2++){
		std::cout<< itr2->first <<":";
		std::cout<<(*itr2).second <<std::endl;
	}

	//way- 3
	std::map<int,std::string> fplayer1;
	fplayer1.insert(std::pair<int,std::string>{10:"Messi"};
	fplayer1.insert(std::pair<int,std::string>{7:"Ronaldo"};
	fplayer1.insert(std::pair<int,std::string>{15:"MBappe"};
	
	return 0;
}

