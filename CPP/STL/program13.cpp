//multiset
#include<iostream>
#include<set>
int main(){
	std::multiset<int> sobj={10,20,40,30,20,50,70,10};
	std::multiset<int>::iterator itr;
	for(itr=sobj.begin();itr!=sobj.end();itr++){
		std::cout<< *itr <<std::endl;
	}
	std::cout<<"For String Values: "<<std::endl;
	std::multiset<std::string,std::greater<std::string>> sobj2={"Kanha","Ashish","Rahul","Badhe"};
	std::multiset<std::string>::iterator itr1;
	for(itr1=sobj2.begin();itr1!=sobj2.end();itr1++){
		std::cout<< *itr1 <<std::endl;
	}


	return 0;
}
