//queue
#include<iostream>
#include<queue>
void showQueue(std::priority_queue<int>& obj){
	int k=obj.size();
	for(int i=0;i<k;i++){

		std::cout<<obj.top()<<std::endl;
		obj.pop();
	}
}
int main(){	
	std::priority_queue<int> que;
	que.push(10);
	que.push(30);
	que.push(20);

	showQueue(que);
	return 0;
}
