//Stack
#include<iostream>
#include<stack>
void showStack(std::stack<int>& obj){
	int k=obj.size();
	for(int i=0;i<k;i++){
		std::cout<<obj.top()<<std::endl;
		obj.pop();
	}
}
int main(){
	std::stack<int> stk;
	stk.push(10);
	stk.push(30);
	stk.push(20);
	stk.push(40);
	showStack(stk);
	return 0;
}
