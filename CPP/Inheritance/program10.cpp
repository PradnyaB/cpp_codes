#include<iostream>
class Parent{
	int x=10;
	public:
	Parent(){
		std::cout<<"In Parent constructor"<<std::endl;
	}
	void getData(){
		std::cout<< " Parent x="<<x <<std::endl;
	}
};
class Child:public Parent{
	int x=10;
	public:
	Child(){
		std::cout<<"In Child constructor"<<std::endl;
	}
	void getData(){
		Parent::getData();
		std::cout<< "Child x="<<x <<std::endl;
	}
};
int main(){
	Child obj;
	obj.getData();
	//(Parent(obj)).getData();
	//obj.Parent::getData();

	return 0;
}
