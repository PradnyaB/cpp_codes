#include<iostream>
class Parent{
	int x=10;
	public:
	Parent(){
		std::cout<<"In Constructor"<<std::endl;
	}

	friend std::ostream& operator<<( std::ostream& out,const Parent& obj2){
		out<<"Parent";
		out<<obj2.x;
		return out;
	}
};
class Child:public Parent{
	int x=20;
	public:
	friend std::ostream& operator<<( std::ostream& out,const Child& obj2){
		out<<"Child";
		out<<obj2.x;
		return out;
	}

};
int main(){
	Child obj;
	std::cout<<(const Parent&)obj<<std::endl;  //No call to copy
	std::cout<<(const Parent)obj<<std::endl;   //call to copy
	std::cout<<obj<<std::endl;
	return 0;
}
