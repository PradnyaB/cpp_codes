#include<iostream>
class Parent{
	int x=10;
	protected:
	int y=20;
	public:
	int z=30;
	void getData(){
		std::cout<<x<< " "<<y<<" "<<z<<std::endl;
	}
};
class Child:public Parent {
	using Parent::getData;
       	//made public to private scope
//	void getData=delete;
	public:
	using Parent::y;       //made protected to public scope
};
int main(){
	Child obj;
	std::cout<<obj.y <<" "<<obj.z<<std::endl;
	obj.getData();
	return 0;
}

