#include<iostream>
class Parent1{
	int x=10;
	public:
		Parent1(){
			std::cout<<"Constructor Parent1"<<std::endl;
		}
		void getData(){
			std::cout<< x <<std::endl;
		}
};
class Parent2{
	int x=20;
	public:
		Parent2(){
			std::cout<<"Constructor Parent2"<<std::endl;
		}
		void getData(){
			std::cout<< x <<std::endl;
		}
};
class Child:public Parent1,public Parent2{
	public:
		Child(){
			std::cout<<"Constructor Child"<<std::endl;
		}
		void getData(){
			std::cout<<"In Child getData"<<std::endl;
			Parent1::getData();
			Parent2::getData();
		}
};
int main(){
	Child obj;
	obj.getData();
	obj.Parent1::getData();
	obj.Parent2::getData();
	return 0;
}
