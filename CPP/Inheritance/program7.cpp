#include<iostream>
class Parent{
	int x=10;
	int y=20;
	public:
	Parent(int x,int y){
		this->x=x;
		this->y=y;
	}
	void getData(){
		std::cout<< x << " "<<y<<std::endl;
	}

};
class Child:public Parent{
	int z=30;
	public:
	Child(){
		std::cout<<"In child Constructor"<<std::endl;
	}
	void printData(){
		std::cout<< z <<std::endl;
	}
}
int main(){
	Child obj(40,50,60);
	obj.getData();
	obj.printData();
	retrun 0;
}

