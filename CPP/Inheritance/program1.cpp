//Composition
#include<iostream>
class Employee{
	std::string EName="Kanha";
	int EmpId=255;
	public:
	Employee(){
		std::cout<<"In Employee Constructor"<<std::endl;
	}
	void setName(std::string Name){
		this->EName=Name;
	}
	void setEmpId(int EmpId){
		this->EmpId=EmpId;
	}

	void getData(){
		std::cout<<EName<<":"<< EmpId<<std::endl;
		
	}
	~Employee(){
		std::cout<<"In Employee Destructor"<<std::endl;
	}

};
class Company{
	std::string CName="Veritas";
	int Strength=5000;
	Employee obj;
	public:
	Company(std::string CName,int Strength){
		this->CName=CName;
		this->Strength=Strength;
		obj.setName("Pradnya");
		obj.setEmpId(8);
	}
	//obj.setName("Pradnya");
//	obj.setEmpId(8);     Error
	void getData(){
	//	obj.setName("Pradnya");  //No Error
	//	obj.setEmpId(8);
		std::cout<< CName<<":"<<Strength<<std::endl;
		obj.getData();
	}
	~Company(){
		std::cout<<"In Company Destructor"<<std::endl;
	}

};

int main(){
	Company obj("Pubmatic",2000);
	obj.getData();
	return 0;
}

	
