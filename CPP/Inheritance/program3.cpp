#include<iostream>
class Parent{
	public:
		Parent(){
			std::cout<<"In Parent constructor"<<std::endl;
		}
		~Parent(){
			std::cout<<"In Parent Destructor"<<std::endl;
		}
		void* operator new(size_t size){
			return void*malloc(size);
		}
};
class Child:Parent{
	public:
		Child(){
			std::cout<<"In child Constructor"<<std::endl;
		}
		~Child(){
			std::cout<<"In Child Destructor"<<std::endl;
		}
};
int main(){
	Child obj;
	return 0;
}
