#include<iostream>
class Parent{
	int x=10;
	public:
	int y=20;
	protected:
	int z=30;
	public:
	Parent(){
	}
	Parent(int x,int y,int z){
		this->x=x;
		this->y=y;
		this->z=z;
	}
	void getData(){
		std::cout<< x <<" "<<y<<" "<< z <<std::endl;
	}


};
class Child:public Parent{
	public:
		Child(int x,int y,int z){
			
			Parent(x,y,z);
		}
		void getInfo(){
			std::cout<<y <<" "<<z<<std::endl;
			getData();
		}
};

int main(){
	Child obj(40,50,60);
	obj.getInfo();
	return 0;
}

