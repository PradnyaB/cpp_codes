#include<iostream>
class Parent{
	int x=10;
	int y=20;
	public:
	Parent(){
		std::cout<< "In no args"<<std::endl;
	}
	Parent(int x, int y){
		std::cout<<"In Parent Para Constructor"<<std::endl;
		std::cout<<"Parent para this="<<this<<std::endl;
		this->x=x;
		this->y=y;
	}
	void getData(){
		std::cout<< x <<std::endl;
		std::cout<< y <<std::endl;
	}
};
class Child:public Parent{
	int z=30;
	public:
	Child(int x,int y,int z):Parent(x,y){
//		Parent(x,y);
		this->z=z;
		std::cout<<"In child Constructor"<<std::endl;
		std::cout<<"child this="<<this<<std::endl;
	}
};
int main(){
	Child obj(40,50,60);
	obj.getData();
	return 0;
}
		


