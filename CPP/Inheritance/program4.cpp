#include<iostream>
class Parent{
	public:
		Parent(){
			std::cout<<"In Parent Constructor"<<std::endl;
		}
		~Parent(){
			std::cout<<"In Parent Destructor"<<std::endl;
		}
};
class Child:public Parent{
	public:
		Child(){
			std::cout<<"In child Constructor"<<std::endl;
		}
		~Child(){
			std::cout<<"In Child Destructor"<<std::endl;
		}
		friend void* operator new(size_t size){
			std::cout<<"In our new  fun"<<std::endl;
			return (void*)malloc(size);
		}
		void operator delete(void* ptr){
			std::cout<<"In our Delete fun"<<std::endl;
			free(ptr);
		}
};
int main(){
	Child *obj1=new Child();
	delete obj1;
	return 0;
}

