#include<iostream>
class Parent{
	int x=10;
	int y=20;
	public:
	int a=30;
	Parent(){
		std::cout<<"In Parent constructor"<<std::endl;
	}
	~Parent(){
		std::cout<<"In Parent Destructor"<<std::endl;
	}

	void getData(){
		std::cout<< x <<" "<<y<<" "<<a<<std::endl;
	}
};
class Child:public Parent{
	int a=40;
	public:
	Child(){
		std::cout<<"In child Constructor"<<std::endl;
	}
	~Child(){
		std::cout<<"In child destructor"<<std::endl;
	}
	void printData(){
		std::cout<< a <<" "<<std::endl;
	}

};
int main(){
	Child obj1;
	
	obj1.getData();
	obj1.printData();
	return 0;
}
