#include<iostream>
template<typename T, typename U>

auto min(T x ,U y){
	/*if(x<y){
		return x;
	}else{
		return y;
	}*/                 // Error inconsistent return type
	return (x<y)? x: y;
}
int main(){
	std::cout<<min(10,15.5f)<<std::endl;
	std::cout<<min(10.5f,7)<<std::endl;
	std::cout<<min<char>('A',100)<<std::endl;
}
