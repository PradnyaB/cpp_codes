#include<iostream>
template<typename T>
T min(T x, T y);
template<>
int min<int>(int x, int y){
	std::cout<<"int Template"<<std::endl;
	return (x<y)? x:y;
}
int min(int x, int y){
	std::cout<<"int Normal"<<std::endl;
	 
	return (x<y)? x:y;
}
template<>
float min<float>(float x, float y){
	return (x<y)? x:y;

}
template<>
char min<char>(char x,char y){
	return (x<y)? x:y;
}
template<>
double min<double>(double x,double y){
	return (x<y)? x:y;
}
int main(){
	std::cout<<min<char>('A','B')<<std::endl;
	std::cout<< min(10,20)<<std::endl;
	std::cout<<min<>(10,20)<<std::endl;
	std::cout<<min<int>(10,20)<<std::endl;
	std::cout<<min<double>(10.5,20.5)<<std::endl;
	std::cout<<min<float>(10.5f,20.5f)<<std::endl;

	return 0;
}
